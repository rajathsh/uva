#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
  int n;
  long int a, b;
  scanf("%d", &n);
  while (n--) {
    scanf("%ld %ld", &a, &b);
    if (a < b) {
	    printf("<\n");
    } else if (a > b) {
	    printf(">\n");
    } else {
	    printf("=\n");
    }
  }
  return 0;
}
