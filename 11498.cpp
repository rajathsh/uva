#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
  int n;
  int a, b;
  int x, y;
  scanf("%d", &n);
  while (n) {
  scanf("%d %d", &a, &b);
  while (n--) {
    scanf("%d %d", &x, &y);
    if (x == a || y == b) {
      printf("divisa\n");
    } else if (x > a && y > b) {
      printf("NE\n");
    } else if (x > a && y < b) {
      printf("SE\n");
    } else if (x < a && y > b) {
      printf("NO\n");
    } else if (x < a && y < b) {
      printf("SO\n");
    }
  }
  scanf("%d", &n);
  }
  return 0;
}
