#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
  int row, col;
  int n;
  int eff_rows;
  int eff_cols;
  int extra_row;
  int extra_col;
  int result;
  scanf("%d", &n);
  while (n--) {
    scanf("%d %d", &row, &col);
    eff_rows = row - 2;
    eff_cols = col - 2;
    extra_row = eff_rows%3?1:0;
    extra_col = eff_cols%3?1:0;
    result = (((eff_rows)/3)+extra_row) * (((eff_cols)/3)+extra_col);
    printf("%d\n", result);
  }
  return 0;
}
