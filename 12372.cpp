#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>

using namespace std;

int main()
{
  int n, num=0;
  int l, h, w;
  scanf("%d", &n);
  while (n--) {
    num++;
    scanf("%d %d %d", &l, &h, &w);

    if (l <= 20 && h <= 20 && w <= 20) {
            printf("Case %d: good\n", num);
    } else {
            printf("Case %d: bad\n", num);
    }
  }
  return 0;
}
