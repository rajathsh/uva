#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <iostream>
#include <cfloat>
using namespace std;


int main()
{
	int reqs, proposals;
	string r[100];
	string p[100];
	string r_met;
	float price;
	int reqs_met;
	float compliance;
    float max_compliance;
	float min_price;
	string min_proposal;
	int rfp = 0;

	cin >>reqs >>proposals;
	while (reqs != 0 && proposals != 0) {
		rfp++;
		min_price = FLT_MAX;
		max_compliance = FLT_MIN;

		for (int i = 0; i < reqs; i++) {
			cin.ignore();
			getline(cin, r[i]);
		}

		for (int i = 0; i < proposals; i++) {
			getline(cin, p[i]);
			scanf("%f %d\n", &price, &reqs_met);;
			for (int j = 0; j < reqs_met; j++) {
				getline(cin, r_met);
			}

			compliance = (float)reqs_met/(float)reqs;

			if (compliance > max_compliance) {
				max_compliance = compliance;
				min_price = price;
				min_proposal = p[i];
			} else if (compliance == max_compliance) {
				if (price < min_price) {
					min_price = price;
					min_proposal = p[i];
				}
			}
		}

		if (rfp > 1) {
			cout << endl;
		}

		cout << "RFP #" << rfp << endl << min_proposal << endl;
		

		cin >>reqs >>proposals;
	}
}