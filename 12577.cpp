#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
        int n = 0;
        string str;

        while (getline(cin, str))
        {
                n++;

                if (str.compare("Hajj") == 0) {
                        printf("Case %d: Hajj-e-Akbar\n", n);
                }else if (str.compare("Umrah") == 0) {
                        printf("Case %d: Hajj-e-Asghar\n", n);
                } else if (str.compare("*") == 0) {
                        break;
                }
        }
        return 0;
}
