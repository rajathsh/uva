#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>

using namespace std;

int main()
{
  int n, num=0;
  unsigned int x[3];
  scanf("%d", &n);
  while (n--) {
    num++;
    scanf("%u %u %u", &x[0], &x[1], &x[2]);

    vector<unsigned int> v(x, x+3);
    sort(v.begin(), v.end());
    
    printf("Case %d: %u\n", num, v[1]);
  }
  return 0;
}
