#include <cstdio>
#include <cstdlib>
#include <climits>
using namespace std;

int main()
{
        int n, num, contestants, max, cur, prev;
        bool is_inc, is_dec;

        scanf("%d", &n);
        printf("Lumberjacks:\n");

        for (num = 1; num <= n; num++) {
                scanf("%d", &prev);
                is_inc = true;
                is_dec = true;

                for (int i = 1; i < 10; i++) {
                        scanf("%d", &cur);

                        if (is_inc && prev > cur) {
                                is_inc = false;
                        }

                        if (is_dec && prev < cur) {
                                is_dec = false;
                        }
                        prev = cur;
                }
                if (is_dec || is_inc) {
                        printf("Ordered\n");
                } else {
                        printf("Unordered\n");
                }
        }
        return 0;
}
