#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <iostream>
using namespace std;

int main()
{
	int n, operations, capacity;
	int dev_status[21][2];
	int device;
	int current;
	int max_current;
	int sequence = 0;
	bool fuse_blown;

	scanf("%d %d %d", &n, &operations, &capacity);
	while (n != 0 && operations != 0 && capacity != 0) {
		sequence++;
		for (int i = 1; i <= n; i++) {
			scanf("%d", &dev_status[i][0]);
			dev_status[i][1] = 0;
		}
		current = 0;
		max_current = INT_MIN;
		fuse_blown = false;
		for (int i = 0; i < operations; i++) {
			scanf("%d", &device);

			if (dev_status[device][1] == 0) {
				dev_status[device][1] = 1;
				current += dev_status[device][0];
			} else {
				dev_status[device][1] = 0;
				current -= dev_status[device][0];
			}

			if (current > max_current) {
				max_current = current;
			}

			if (current > capacity) {
				fuse_blown = true;
			}
		}

		if (fuse_blown) {
			printf("Sequence %d\n", sequence);
			printf("Fuse was blown.\n");
		} else {
			printf("Sequence %d\n", sequence);
			printf("Fuse was not blown.\n");
			printf("Maximal power consumption was %d amperes.\n", max_current);
		}
		printf("\n");
		scanf("%d %d %d", &n, &operations, &capacity);
	}
}