#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <climits>
using namespace std;


int main()
{
	int n;
	char str[1000010];

	long digit;
	long num_digits;
	int i;
	char c;
	int len;
	int mem[100] = {0};

	scanf("%d", &n);

	for(int i = 1; i <= n; i++) {
		scanf("%s", str);
		len = strlen(str);

		int k = 0;
		for (int j = 0; j < len; j++){
			c = str[j];

			switch (c) {
				case '>':
				    if (k == 99) {
				    	k = 0;
				    } else {
				    	k++;
				    }
				    break;
				case '<':
				    if (k == 0) {
				    	k = 99;
				    } else {
				    	k--;
				    }

				    break;
				case '+':
				    mem[k]++;
				    if (mem[k] == 256) {
				    	mem[k] = 0;
				    }
				    break;
				case '-':
				    mem[k]--;
				    if (mem[k] == -1) {
				    	mem[k] = 255;
				    }
				    break;
				default:
				    break;
			}
		}
		printf("Case %d:", i);
		for (int l = 0; l < 100; l++) {
			printf(" %02X", mem[l]);
			mem[l] = 0;
		}
		printf("\n");
	}
	return 0;
}