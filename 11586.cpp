#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;


int main()
{
	int n;
	int size;
	string str;
	int m, f;

	scanf("%d\n", &n);

	for(int i = 0; i < n; i++) {

		getline(cin, str);
		size = str.size();
		m = f = 0;
		for (int j = 0; j < size; j++) {
			if (str.at(j) == 'M') {
				m++;
			} else if (str.at(j) == 'F') {
				f++;
			}
		}
		if (m == f && m > 1) {
			printf("LOOP\n");
		} else {
			printf("NO LOOP\n");
		}
	}
	return 0;
}