#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
        int n;
        int farmers;
        int area;
        int animals;
        int env;
        long cost;

        scanf("%d", &n);

        while (n--) {
                scanf("%d", &farmers);
                cost = 0;

                while (farmers--) {
                        scanf("%d %d %d", &area, &animals, &env);

                        cost += area * env;
                }
                printf("%ld\n", cost);
        }

        return 0;
}
