#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
  int a, b, c, d;
  while (scanf("%d %d %d %d", &a, &b, &c, &d) != EOF) {
    if (a == 0 && b == 0 && c == 0 && d == 0) return 0;
    int x = (a-b)>0?(a-b):(40-abs(a-b));
    int y = (c-b)>0?(c-b):(40-abs(c-b));
    int z = (c-d)>0?(c-d):(40-abs(c-d));
    int result = 40 + 40 + 40 + x + y + z;
    printf("%d\n", result*9);
  }
  return 0;
}
