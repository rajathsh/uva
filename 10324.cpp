#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <iostream>
#include <cfloat>
using namespace std;


int main()
{
	char str[2000000];
	int n, a, b;
	int case_num = 0;
	bool same_chars;
	int min, max;

	while (scanf("%s", str) == 1) {

		case_num++;

		scanf("%d", &n);

		cout << "Case " << case_num << ":" << endl;

		for (int i = 0; i < n; i++) {
			scanf("%d %d", &a, &b);

			min = a>b?b:a;
			max = a>b?a:b;

			same_chars = true;
			char c = str[min];
			for (int j = min; j <= max && same_chars; j++) {
				if (str[j] != c) {
					same_chars = false;
				}

			}

			if (same_chars) {
				cout << "Yes" << endl;
			} else {
				cout << "No" << endl;
			}
		}
	}
	return 0;
}