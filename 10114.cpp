#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
  int duration, no_records; 
  double down_pmt;
  int months = 0;
  int month;
  double prev_depreciation;
  double depreciation;
  double money_owed;
  double car_worth;
  double monthly_payment;
  int result_month;
  bool result;

  while (scanf("%d %lf %lf %d", &duration, &down_pmt, &car_worth, &no_records)) {
          if (duration < 0) break;
          months = 0;
          month = 0;
          prev_depreciation = 0.0;
          result = false;
          monthly_payment = car_worth/duration;
          money_owed = car_worth;
          car_worth += down_pmt;
          while (no_records--) {
                  scanf("%d %lf", &month, &depreciation);

                  if (month == 0) {
                          car_worth = car_worth - (car_worth * depreciation);
                          if (result == false && money_owed < car_worth) {
                                  result_month = months;
                                  result = true;
                          }
                          prev_depreciation = depreciation;
                          months++;
                  } else {
                          while (result == false && months < month) {
                                  car_worth = car_worth - (car_worth * prev_depreciation);
                                  money_owed -= monthly_payment;
                                  if (result == false && money_owed < car_worth) {
                                          result_month = months;
                                          result = true;
                                  }
                                  months++;
                          }
                          if (result == false && months == month) {
                                  car_worth = car_worth - (car_worth * depreciation);
                                  prev_depreciation = depreciation;
                                  money_owed -= monthly_payment;
                                  if (result == false && money_owed < car_worth) {
                                          result_month = months;
                                          result = true;
                                  }
                                  months++;
                          }
                  }

          }
          while (result == false && months <= duration) {
                  car_worth = car_worth - (car_worth * depreciation);
                  money_owed -= monthly_payment;
                  if (result == false && money_owed < car_worth) {
                          result_month = months;
                          result = true;
                  }
                  months++;
          }
          if (result == true) {
                  printf("%d %s\n", result_month, result_month == 1? "month":"months");
          }
  }
  return 0;
}
