#include <cstdio>
#include <cstdlib>
#include <climits>
using namespace std;

int main()
{
        int n, num, mile, juice, calls, call_rate;

        scanf("%d", &n);

        for (num = 1; num <= n; num++) {

                scanf("%d", &calls);
                mile = juice = 0;

                for (int i = 0; i < calls; i++) {
                        scanf("%d", &call_rate);
                        mile += (call_rate/30) * 10  + 10;
                        juice += (call_rate/60) * 15  + 15;

                }

                if (mile < juice) {
                        printf("Case %d: Mile %d\n", num, mile);
                } else if (mile == juice) {
                        printf("Case %d: Mile Juice %d\n", num, juice);
                } else {
                        printf("Case %d: Juice %d\n", num, juice);
                }

        }
        return 0;
}
