#include <cstdio>
#include <cstdlib>
#include <climits>

using namespace std;

int main()
{

        int n;
        int budget;
        int hotels;
        int weeks;
        int cost_per_night;
        int beds_available;
        int max_beds;
        int total_cost;
        bool accomodation_possible;
        int min_cost;
        while (scanf("%d %d %d %d", &n , &budget, &hotels, &weeks) == 4) {
                max_beds = 0;
                accomodation_possible = false;
                min_cost = INT_MAX;
                while (hotels--) {
                        scanf("%d", &cost_per_night);
                        int w = weeks;
                        while(w--) {
                                scanf("%d", &beds_available);
                                max_beds = beds_available > max_beds? beds_available : max_beds;
                        }

                        if (max_beds >= n) {
                                total_cost = n * cost_per_night;
                                if (total_cost <= budget) {
                                        if (min_cost > total_cost) {
                                                min_cost = total_cost;
                                        }
                                        accomodation_possible = true;
                                }
                        }
                }
                if (accomodation_possible == true) {
                        printf("%d\n", min_cost);
                } else {
                        printf("stay home\n");
                }
        }

        return 0;
}
