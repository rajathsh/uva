#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
        int n;
        int north, south;
        int cols;
        int diff;
        int prev_diff;
        bool gap_close, first;

        scanf("%d\n", &n);

        bool line_break = false;
        while (n--) {
                scanf("%d", &cols);
                gap_close = true;
                first = true;
                while (cols--) {
                        scanf("%d %d", &north, &south);

                        diff = north - south;

                        if (first == true) {
                                prev_diff = diff;
                                first = false;
                        }

                        if (prev_diff != diff) {
                                gap_close = false;
                        }
                        prev_diff = diff;
                }
                if (line_break == true) {
                        printf("\n");
                } else {
                        line_break = true;
                }
                if (gap_close == false) {
                        printf("no\n");
                } else {
                        printf("yes\n");
                }
        }

        return 0;
}
