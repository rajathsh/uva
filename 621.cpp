#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
        int n;
        int size;
        string str;

        scanf("%d\n", &n);

        while (n--)
        {
                getline(cin, str);
                size = str.size();

                if (str.compare("1") == 0 || str.compare("4") == 0 || str.compare("78") == 0) {
                        printf("+\n");
                }else if (size >= 2 && str[size-2] == '3' && str[size-1] == '5'){
                        printf("-\n");
                }else if (size >= 2 && str[0] == '9' && str[size-1] == '4'){
                        printf("*\n");
                }else if (size >= 3 && str[0] == '1' && str[1] == '9' && str[2] == '0'){
                        printf("?\n");
                }
        }
        return 0;
}
