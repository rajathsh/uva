#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <iostream>
using namespace std;

int main()
{
        int n, num_inst, inst[101], sum;
        string str;

        scanf("%d\n", &n);

        for (int i = 0; i < n; i++) {
                scanf("%d\n", &num_inst);
                sum = 0;

                for(int j = 1; j <= num_inst; j++) {
                        getline(cin, str);

                        if (str.compare("LEFT") == 0) {
                                inst[j] = -1;

                        } else if (str.compare("RIGHT") == 0) {
                                inst[j] = 1;
                        } else {
                                inst[j] = inst[stoi(str.substr(str.find_last_of(' ')+1), NULL, 10)];
                        }
                        sum += inst[j];
                }
                printf("%d\n", sum);
        }
        
                
        return 0;
}
