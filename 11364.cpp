#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
  int n, no_shops, shop;
  int r, l, result;
  scanf("%d", &n);
  while (n--) {
    scanf("%d", &no_shops);
    r = -1;
    l = 100;
    while (no_shops--) {
      scanf("%d", &shop);
      if (shop >= r) {
        r = shop;  
      }
      if (shop <= l) {
        l = shop;
      }
    }
    result = 2*(r-l);
    printf("%d\n", result);
  }
  return 0;
}
