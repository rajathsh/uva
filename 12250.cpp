#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <iostream>

using namespace std;

int main()
{
  string str;
  string lang;
  int n = 0;
  while(getline(cin, str)) {
    n++;
    if (str.compare("HELLO") == 0) {
      lang = "ENGLISH";
    } else if (str.compare("HOLA") == 0) {
      lang = "SPANISH";
    } else if (str.compare("HALLO") == 0) {
      lang = "GERMAN";
    } else if (str.compare("BONJOUR") == 0) {
      lang = "FRENCH";
    } else if (str.compare("CIAO") == 0) {
      lang = "ITALIAN";
    } else if (str.compare("ZDRAVSTVUJTE") == 0) {
      lang = "RUSSIAN";
    } else if (str.compare("#") == 0) {
      return 0;
    } else {
      lang = "UNKNOWN";
    }

    printf("Case %d: %s\n", n, lang.c_str());
  } 
  return 0;
}
