#include <cstdio>
#include <cstdlib>
#include <climits>
using namespace std;

int main()
{
        int n, num, no_walls, cur, prev, l, h;

        scanf("%d", &n);

        for (num = 1; num <= n; num++) {
                scanf("%d", &no_walls);
                l = 0;
                h = 0;

                scanf("%d", &prev);
                for (int i = 1; i < no_walls; i++) {
                        scanf("%d", &cur);

                        if (cur > prev) {
                                h++;
                        } else if (prev > cur) {
                                l++;
                        }
                        prev = cur;
                }
                printf("Case %d: %d %d\n", num, h, l);
        }
}
