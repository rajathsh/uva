#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <climits>
#include <iostream>
using namespace std;

long get_num_of_digits(long n)
{
	long num_digits = 0;

	if (n == 0) num_digits++;

	while (n%10) {
		n = n/10;
		num_digits++;
	}

	return num_digits;
}

int main()
{
	string str;
	long digit;
	long num_digits;
	int i;

	cin >> str;

	while(str.compare("END") != 0) {
		i = 1;
		if (str.size() == 1) {
			digit = strtol(&str[0], NULL, 10);
		} else {
			i = 1;
			digit = str.size();
			i++;
		}

		num_digits = get_num_of_digits(digit);
		while (digit != num_digits) {
			digit = num_digits;
			num_digits = get_num_of_digits(digit);
			i++;
		}
		printf("%d\n", i);
		cin >> str;
	}
	return 0;
}