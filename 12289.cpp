#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>

using namespace std;

int main()
{
        int n;
        string str;

        scanf("%d\n", &n);

        while (n--)
        {
                getline(cin, str);

                if (str.size() == 5) {
                        printf("3\n");
                }else {
                        if ((str[0] == 'o' && str[1] == 'n') ||
                            (str[1] == 'n' && str[2] == 'e') ||
                            (str[0] == 'o' && str[2] == 'e')) {
                                printf("1\n");
                        } else if ((str[0] == 't' && str[1] == 'w') ||
                            (str[1] == 'w' && str[2] == 'o') ||
                            (str[0] == 't' && str[2] == 'o')) {
                                printf("2\n");
                        }

                }
        }
        return 0;
}
