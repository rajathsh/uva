#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <iostream>
#include <cfloat>
using namespace std;

int sum_of_digits(int n) {
	if (n == 0) return 0;

	return (n%10 + sum_of_digits(n/10));
}

int sum_of_char_digits(string name, int len) {
	int digits = 0;
	for (int i = 0; i < len; i++) {
		if (name.at(i) >= 'a' && name.at(i) <= 'z') {
			digits += name.at(i) - 'a' + 1;
		}
		if (name.at(i) >= 'A' && name.at(i) <= 'Z') {
			digits += name.at(i) - 'A' + 1;
		}
	}
	while (digits/10) {
		digits = sum_of_digits(digits);
	}
	return (digits);
}
int main()
{
	string name1, name2;
	int len1, len2;
	float ratio;
	int a, b;

	while (getline(cin, name1)) {
		getline(cin, name2);

		len1 = name1.size();
		len2 = name2.size();

		a = sum_of_char_digits(name1, len1);
		b = sum_of_char_digits(name2, len2);

		if (a > b) {
			int temp = a;
			a = b;
			b = temp;
		}

		ratio = ((float)a*100/(float)b);
		printf("%0.2f %%\n", ratio);

		
	}
	return 0;
}