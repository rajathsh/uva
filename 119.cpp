#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <iostream>
#include <map>
using namespace std;


int main()
{
	int n;
	string name;
	int gift, gift_per_person;
	int giftees;
	string giftee;
	bool first = true;
	string names[100];
	map<string, int> worth;

	while (cin >> n) {
		for(int i = 0; i < n; i++) {
			cin >> names[i];
			worth[names[i]] = 0;
		}

		for (int i = 0; i < n; i++) {
			
			cin >> name;
			cin >>gift;
			cin >> giftees;

			if (giftees > 0) {
				
				gift_per_person = (gift/giftees);
				worth[name] -= (gift_per_person * giftees);
			}

			for (int j = 0; j < giftees; j++) {
				cin >> giftee;
		  		if (gift > 0) {
				    worth[giftee] += gift_per_person;
			    }
			}
		}
		if (first) {
			first = false;
		} else {
			cout << endl;
		}
		for (int i = 0; i < n; i++) {
			cout << names[i] << " " << worth[names[i]] << endl;
		}
	}
}