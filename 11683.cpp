#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <climits>
#include <iostream>
using namespace std;


int main()
{
	int h, l;
	int ht;
	int total_run;
	int prev_ht;

	scanf("%d", &h);

	while(h != 0) {
		scanf("%d", &l);
		total_run = 0;
		scanf("%d", &ht);
		total_run = h - ht;
		prev_ht = ht;
		for (int i = 1; i < l; i++) {
			scanf("%d", &ht);

			if (prev_ht > ht) {
				total_run += prev_ht - ht;
			}
			prev_ht = ht;
		}
		printf("%d\n", total_run);


		scanf("%d", &h);
	}
	return 0;
}