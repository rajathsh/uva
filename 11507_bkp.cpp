#include <cstdio>
#include <cstdlib>

int main()
{
	int n;
	int dir;
	char bend_dir[3];
	char dir_map[7][4] = {"\0", "+x\0", "-x\0", "+y\0", "-y\0", "+z\0", "-z\0"};

	

	scanf("%d", &n);

	while(n) {
		dir = 1;
		for (int i = 0; i < n-1; i++) {
			scanf("%s", bend_dir);

			if (bend_dir[0] == 'N') continue;

			if (bend_dir[0] == '+' && bend_dir[1] == 'y') {
				if (dir == 1) {
					dir = 3;
				} else if (dir == 2) {
					dir = 4;
				} else if (dir == 3) {
					dir = 2;
				} else if (dir == 4) {
					dir = 1;
				} else if (dir == 5) {
					dir = 5;
				} else if (dir == 6) {
					dir = 6;
				}
			} else if (bend_dir[0] == '-' && bend_dir[1] == 'y') {
				if (dir == 1) {
					dir = 4;
				} else if (dir == 2) {
					dir = 3;
				} else if (dir == 3) {
					dir = 1;
				} else if (dir == 4) {
					dir = 2;
				} else if (dir == 5) {
					dir = 5;
				} else if (dir == 6) {
					dir = 6;
				}

			} else if (bend_dir[0] == '+' && bend_dir[1] == 'z') {
				if (dir == 1) {
					dir = 5;
				} else if (dir == 2) {
					dir = 6;
				} else if (dir == 3) {
					dir = 3;
				} else if (dir == 4) {
					dir = 4;
				} else if (dir == 5) {
					dir = 2;
				} else if (dir == 6) {
					dir = 1;
				}
			} else if (bend_dir[0] == '-' && bend_dir[1] == 'z') {
				if (dir == 1) {
					dir = 6;
				} else if (dir == 2) {
					dir = 5;
				} else if (dir == 3) {
					dir = 3;
				} else if (dir == 4) {
					dir = 4;
				} else if (dir == 5) {
					dir = 1;
				} else if (dir == 6) {
					dir = 2;
				}
			} 

		}
		printf("%s\n", dir_map[dir]);

		scanf("%d", &n);
	}
	return 0;
}