#include <cstdio>
#include <cstdlib>
#include <climits>
#include <stdio.h>
#include <string.h>
using namespace std;

int main()
{
        int B, N;
        int b[21];
        bool solvent;
        int D, C, d;


        scanf("%d %d", &B, &N);

        while (B && N) {
                solvent = true;
                for (int i = 1; i <= B; i++) {
                        scanf("%d", &b[i]);
                }

                for (int i = 1; i<= N; i++) {
                        scanf("%d %d %d", &D, &C, &d);

                        b[D] = b[D] - d;
                        b[C] = b[C] + d;
                }
                for (int i = 1; i <= B; i++) {
                        if (b[i] < 0) {
                                solvent = false;
                        }
                }
                if (solvent == true) {
                        printf("S\n");
                } else {
                        printf("N\n");
                }
                scanf("%d %d", &B, &N);
        }

        return 0;
}
