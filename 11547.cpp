#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
  int n;
  int x;
  long int result;
  scanf("%d", &n);
  while (n--) {
    scanf("%d", &x);
    result = ((((((x * 567)/9)+7492)*235)/47)-498);
    result /= 10;
    result %= 10;
    printf("%ld\n", abs(result));
  }
  return 0;
}
