#include "grader.h"
#include "cluedo.h"

void Solve(){
   int r;
   int m = 1;
   int l = 1;
   int w = 1;
   while(m <= 6 && l <= 10 && w <= 6) {

   	 r = Theory(m, l, w);
   	 if (r == 0) {
   	 	return;
   	 } else if (r == 1) {
   	 	m++;
   	 } else if (r == 2) {
   	 	l++;
   	 } else if (r == 3) {
   	 	w++;
   	 }
   }
}
