#include <cstdio>
#include <cstdlib>
#include <climits>
using namespace std;

int main()
{
        int n, num, contestants, max, cur;

        scanf("%d", &n);

        for (num = 1; num <= n; num++) {
                scanf("%d", &contestants);

                max = INT_MIN;
                for (int i = 0; i < contestants; i++) {
                        scanf("%d", &cur);

                        if (cur > max) {
                                max = cur;
                        }
                }
                printf("Case %d: %d\n", num, max);
        }
        return 0;
}
