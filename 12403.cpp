#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
        int n;
        long donations = 0;
        string str;

        scanf("%d\n", &n);

        while (n--)
        {
                getline(cin, str);

                if (str.compare("report") == 0) {
                        printf("%ld\n", donations);
                }else {
                        donations += strtol(&str[str.find(' ')], NULL, 10);
                }
        }
        return 0;
}
