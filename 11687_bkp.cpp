#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <climits>
#include <iostream>
using namespace std;

long get_num_of_digits(long n)
{
	long num_digits = 0;

	if (n == 0) num_digits++;

	while (n%10) {
		n = n/10;
		num_digits++;
	}

	return num_digits;
}

int main()
{
	char str[1000010];
	long digit;
	long num_digits;
	int i;

	scanf("%s", str);

	while(str[0] != 'E') {
		num_digits = strlen(str);
		if (num_digits == 1 && str[0] == '1') {
			i = 1;
		}else if (num_digits == 1) {
			i = 2;
		} else if (num_digits < 10) {
			i = 3;
		} else {
			i = 4;
		}
		printf("%d\n", i);
		scanf("%s", str);
	}
	return 0;
}