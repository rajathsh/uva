#include <cstdio>
#include <algorithm>

using namespace std;

int main()
{
  int i, j, temp_i, temp_j;
  unsigned int max_cycle_length; 
  while (scanf("%d %d", &i, &j) != EOF) {
    temp_i = i;
    temp_j = j;

    if (i > j) swap(i,j);

    max_cycle_length = 0;
    while (i <= j) {
      unsigned int n = i;
      unsigned int cycle_length = 1;
      while (n != 1) {
        if (n % 2) {
          n = 3 * n + 1;
        } else {
          n /= 2;
        }
        cycle_length++;  
      }
      if (cycle_length > max_cycle_length) {
        max_cycle_length = cycle_length;
      }
      i++;
    }
    printf("%d %d %d\n", temp_i, temp_j, max_cycle_length);

  }
}
