#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
  int n, event;
  int count = 0;
  int result;
  scanf("%d", &n);
  while(n) {
      count++;
      result = 0;
      while (n--) {
          scanf("%d", &event);
          if (event == 0) {
              result--;
          } else {
              result++;
          }
      }
      printf("Case %d: %d\n", count, result);
      scanf("%d", &n);
  }
  return 0;
}
