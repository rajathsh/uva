#include <cstdio>
#include <cstdlib>

using namespace std;

void sum_of_digits(unsigned long n)
{
        unsigned long sum = 0;
        if (n/10 == 0) {
                printf("%lu\n", n);
                return;
        }

        while (n) {
                sum += n%10;
                n = n/10;
        }
        sum_of_digits(sum);
}
int main()
{
        unsigned long n;

        scanf("%lu", &n);
        while (n) {
                sum_of_digits(n);

                scanf("%lu", &n);
        }

        return 0;
}
