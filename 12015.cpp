#include <cstdio>
#include <cstdlib>
#include <climits>
using namespace std;

int main()
{
        int n, num, max;
        char str[10][102];
        int  score[10];

        scanf("%d", &n);

        for (num = 1; num <= n; num++) {
                max = INT_MIN;

                for (int i = 0; i < 10; i++) {
                        scanf("%s %d", str[i], &score[i]);

                        if (score[i] > max) {
                                max = score[i];
                        }

                }
                printf("Case #%d:\n", num);
                for (int i = 0; i < 10; i++) {
                        if (score[i] == max) {
                                printf("%s\n", str[i]);
                        }
                }

        }
        return 0;
}
