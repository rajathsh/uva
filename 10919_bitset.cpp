#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <iostream>
#include <vector>
using namespace std;


int main()
{
	int courses;
	int categories;
	int course;
	
	bool pre_reqs_satisfied;
	int num_courses;
	int reqs;

	

	scanf("%d", &courses);

	while(courses) {
		scanf("%d", &categories);
		
		vector<bool> course_map(10000, false);
		for (int i =0; i < courses; i++) {
			scanf("%d", &course);
			course_map[course] = true;
		}
		pre_reqs_satisfied = true;

		for (int i = 0; i < categories; i++) {
			scanf("%d %d", &num_courses, &reqs);

			for(int j = 0; j < num_courses; j++) {
				scanf("%d", &course);

				if(course_map[course]) {
					reqs--;
				}
			}
			if (reqs > 0) {
				pre_reqs_satisfied = false;
			}
		}
		if (pre_reqs_satisfied) {
			printf("yes\n");
		} else {
			printf("no\n");
		}
		scanf("%d", &courses);
	}
	return 0;
}