#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <iostream>
using namespace std;

int main()
{
	int h, u, d, f;
	float fatigue;
	int days;
	float height_climbed;
	float daily_climb;

	scanf("%d %d %d %d", &h, &u, &d, &f);
	while (h != 0 && u != 0 && d != 0 && f != 0) {
		height_climbed = (float)0.0;

		days = 0;
		daily_climb = (float)u;
		while (height_climbed < (float)h && height_climbed >= (float)0.0)
		{
			if (days == 0) {
				fatigue = 0;
			} else {
				fatigue = (float)(u * f / (float)100.0);
			}
			float initial_height = height_climbed;
			daily_climb -= fatigue;
			if (daily_climb > (float)0.0) {
			    height_climbed += (float)daily_climb;
			}
			float day_height = height_climbed;
			days++;
			if (height_climbed <= (float)h) {
				height_climbed -= (float)d;
			}
			//printf("day: %d ini_ht: %f day_clb: %f ht_cmd: %f af_slg: %f\n",
			//	    days, initial_height, daily_climb, day_height, height_climbed);
			//getchar();
		}
		if (height_climbed < (float)h) {
			printf("failure on day %d\n", days);
		} else {
			printf("success on day %d\n", days);
		}

		scanf("%d %d %d %d", &h, &u, &d, &f);
	}
	return 0;
}