#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <iostream>
using namespace std;

int get_index_of_gifter(char names[100][13], char name[13])
{
	for (int i = 0; i < 100; i++) {
		if(strcmp(names[i], name) == 0) {
			return i;
		}
	}
	return -1;
}

int main()
{
	int n;
	char names[100][13];
	int worth[10];
	char name[13];
	int gift, gift_per_person;
	int giftees;
	char giftee[13];
	bool first = true;

	while (scanf("%d", &n) == 1) {
		for(int i = 0; i < n; i++) {
			scanf("%s", names[i]);
		}

		for (int i = 0; i < 10; i++) {
			worth[i] = 0;
		}

		for (int i = 0; i < n; i++) {
			scanf("%s %d %d", name, &gift, &giftees);
			int index = get_index_of_gifter(names, name);
			
			if (giftees > 0 && gift > 0) {
				gift_per_person = (gift/giftees);
				worth[index] -= ( gift_per_person * giftees);
			}

			for (int j = 0; j < giftees; j++) {
				scanf("%s", giftee);
				int giftee_index = get_index_of_gifter(names, giftee);
		  		if (gift > 0) {
				    worth[giftee_index] += gift_per_person;
			    }
			}
		}
		if (first) {
			first = false;
		} else {
			printf("\n");
		}
		for (int i = 0; i < n; i++) {
			printf("%s %d\n", names[i], worth[i]);
		}
	}
}