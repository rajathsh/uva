#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <iostream>

using namespace std;

int main()
{
  string str;
  char output[2048];
  bool opening_quote = false;
  memset(output, 0, sizeof(output));
  while(std::getline(std::cin, str)) {
    int i = 0;
    int j = 0;
    for(; i < str.length(); i++) {
      if (str[i] == '"') {
        if (opening_quote) {
          output[j++] = '\'';
          output[j++] = '\'';
          opening_quote = false;
        } else {
          output[j++] = '`';
          output[j++] = '`';
          opening_quote = true;
        }
      } else {
        output[j++] = str[i];
      }
    }
    printf("%s\n", output);
  
    memset(output, 0, sizeof(output));
  } 
  return 0;
}
