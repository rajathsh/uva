#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <climits>
#include <iostream>
using namespace std;


int main()
{
	int n;
	int size;
	char str[2000050];
	int dist, min_dist;
	int r, d;

	scanf("%d", &n);

	while(n) {

		scanf("%s", str);
		min_dist = INT_MAX;
		d = -1;
		r = -1;
		for (int i = 0; i < n; i++) {
			if (str[i] == 'D') {
				d = i;

				if (r != -1) {
					dist = abs(r-d);
					if (dist < min_dist) {
						min_dist = dist;
					}
				}
			} else if (str[i] == 'R') {
				r = i;
				if (d != -1) {
					dist = abs(r-d);
					if (dist < min_dist) {
						min_dist = dist;
					}
				}
			} else if (str[i] == 'Z') {
				min_dist = 0;
				break;
			}
		}
		printf("%d\n", min_dist);
		scanf("%d", &n);
	}
	return 0;
}